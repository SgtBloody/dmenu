#+TITLE: dmenu
This is my patched build of dmenu

** Included patches
+ [[https://tools.suckless.org/dmenu/patches/center/][Center]]
+ [[https://tools.suckless.org/dmenu/patches/line-height/][Line Height]]
+ [[https://tools.suckless.org/dmenu/patches/xyw/][xyw (Position and width)]]
+ [[https://tools.suckless.org/dmenu/patches/xresources/][Xresources]]
+ [[https://tools.suckless.org/dmenu/patches/mouse-support/][Mouse Support]]
+ [[https://tools.suckless.org/dmenu/patches/password/][Password]]
